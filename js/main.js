// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-app.js";
import { getFirestore, doc, getDoc, getDocs, collection } from "https://www.gstatic.com/firebasejs/9.4.0/firebase-firestore.js";
import { getDatabase,onValue,ref,set,get,child,update,remove } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-database.js";
import { getStorage, ref as refS, uploadBytes, getDownloadURL } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-storage.js";
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged, signOut} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
// Your web app's Firebase configuration

const firebaseConfig = {
    apiKey: "AIzaSyB3yHQQnCdhBwKRAV_XaD0WdxiMZf9WU6E",
    authDomain: "recuperacionfinal-46885.firebaseapp.com",
    databaseURL: "https://recuperacionfinal-46885-default-rtdb.firebaseio.com",
    projectId: "recuperacionfinal-46885",
    storageBucket: "recuperacionfinal-46885.appspot.com",
    messagingSenderId: "772661284451",
    appId: "1:772661284451:web:d9b6dbf50f55110789f256"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();

var btnInsertar = document.getElementById('btnInsertar');
var btnBuscar = document.getElementById('btnBuscar');
var btnActualizar = document.getElementById('btnActualizar');
var btnBorrar = document.getElementById('btnLimpiar');
var btnTodos = document.getElementById('btnTodos');
var btnLimpiar = document.getElementById('btnLimpiar');
var btnDesconectar = document.getElementById('btnDesconectar');
var lista = document.getElementById('lista');
var productosWeb = document.getElementById('productosAcomodados'); 
var archivo = document.getElementById('archivo');

var Id = "";
var nombreProducto = "";
var descripcion = "";
var precio = "";
var imgNombre = "";
var Url = "";
var estado ="";

if(window.location.href == "https://practica1giancarlo.000webhostapp.com/rExa/html/productos.html"){
    window.onload = mostrarProductos();
}

function mostrarProductos(){
  const db = getDatabase();
  const dbRef = ref(db, 'productos');

  onValue(dbRef, (snapshot) =>{
      if(lista){
        lista.innerHTML = "";
      }
      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val();

        if(lista){
          
          lista.innerHTML =  "<div class='campo'> " +  lista.innerHTML  + "<br/>"+ "Código:" + childKey + "<br/> " + "Producto: " + childData.nombreProducto + "<br/>"+ "Descripción: " + childData.descripcion + "<br/>" + "Precio: " + childData.precio + "<br/>" + "Estado: " + childData.estado + "<br/>" + "Nombre de la imagen: " + childData.imgNombre + "<br/>" + "Url: " + childData.Url + "<br/> </div>";
          
        }
        else if(childData.estado == 0){
       
            productosWeb.innerHTML = "<div class='camposProductos'> " + productosWeb.innerHTML + "<br/>"+ "Código:" + childKey + "<br/> " + "Producto: " + childData.nombreProducto + "<br/>"+ "Descripción: " + childData.descripcion + "<br/>" + "Precio: " + childData.precio + "<br/>" + "<br/>" + "Nombre de la imagen: " + childData.imgNombre + "<br/>" + "<img src=' " + childData.Url + "'></img>" + "<br/>" + "<button class='btnComprar'>Comprar</button>" + "<br/> </div>";
        }

      });
  },{
    onlyOnce: true
  });

}


function leer(){
    Id =document.getElementById('codigo').value;
    nombreProducto = document.getElementById('nombreProducto').value;
    descripcion = document.getElementById('descripcion').value;
    precio = document.getElementById('precio').value;
    imgNombre = document.getElementById('imgNombre').value;
    Url = document.getElementById('url').value;
    estado = document.getElementById("estado").value;
}


function insertarProductos(){
    subirImagen();

    setTimeout(leer, 3000);
    setTimeout(()=>{
      set(ref(db, 'productos/' + Id), {
        nombreProducto: nombreProducto,
        descripcion: descripcion,
        precio: precio,
        imgNombre: imgNombre,
        Url: Url,
        estado : estado
      }).then((response)=>{
        alert("Producto ingresado con Exito");
      }).catch((error)=>{
        alert("Ha ocurrido un error" + error);
      });
    }, 5000);

}




function buscador(){
    document.getElementById('codigo').value = Id;
    document.getElementById('nombreProducto').value = nombreProducto;
    document.getElementById('descripcion').value = descripcion;
    document.getElementById('precio').value = precio;
    document.getElementById('imgNombre').value = imgNombre;
    document.getElementById('url').value =Url;
    document.getElementById('estado').value = estado;

}


function enseñarDatos(){
    leer();
    const dbref = ref(db);

    get(child(dbref, 'productos/' + Id)).then((snapshot)=>{
      if(snapshot.exists()){
        nombreProducto = snapshot.val().nombreProducto;
        descripcion = snapshot.val().descripcion;
        precio = snapshot.val().precio;
        imgNombre = snapshot.val().imgNombre;
        Url = snapshot.val().Url;

        estado = snapshot.val().estado;

        buscador();

      }
      else{
        alert("Error, el producto NO existe");
      }
    }).catch((error)=>{
      alert("Ha surgido un error " + error);
    });




}


var file = "";
var name = "";

function cargarImagen(){

  file = event.target.files[0];
  name = event.target.files[0].name;
  document.getElementById('imgNombre').value = name;

}

function subirImagen(){
  const storage = getStorage();
  const storageRef = refS(storage, 'imagenes/' + name);

  uploadBytes(storageRef, file).then((snapshot) => {
    alert('Se subió correctamente el archivo')
  });

    descargarImagenes();
}


function descargarImagenes(){
  
    // Create a reference to the file we want to download
    const storage = getStorage();
    const starsRef = refS(storage, 'imagenes/' + name);
    
    // Get the download URL
    getDownloadURL(starsRef)
      .then((url) => {
        document.getElementById('url').value = url;
        document.getElementById('imagen').src = url;
      })
      .catch((error) => {
        
        switch (error.code) {
          case 'storage/object-not-found':
            console.log("No existe el archivo");
            break;
          case 'storage/unauthorized':
            console.log("No tiene permisos");
            break;
          case 'storage/canceled':
            console.log("Se cancelo o no hay internet")
            break;
    
          // ...
    
          case 'storage/unknown':
            console.log("Sucedio algo inesperado");
            break;
        }
      });
  
  
  
  }


  function actualizar(){
      subirImagen();
      setTimeout(leer(), 3000)
      setTimeout(()=>{
        update(ref(db,'productos/' + Id),{
          nombreProducto: nombreProducto,
          descripcion: descripcion,
          precio: precio,
          imgNombre: imgNombre,
          Url: Url,
          estado: estado
        }).then(()=>{
          alert("Se ha actualizado el registro con éxito");
          mostrarProductos();
        }).catch(()=>{
          alert("Ha surgido un error " + error);
        })
      })
  }


  function limpiar(){

    Id = "";
    nombreProducto = "";
    descripcion = "";
    precio = "";
    imgNombre = "";
    Url = "";
    estado = "";

     lista.innerHTML = ""; 
    buscador();

}



if(archivo){
  archivo.addEventListener('change', cargarImagen);
}
if(btnInsertar){
  btnInsertar.addEventListener('click', insertarProductos);
}
if(btnBuscar){
  btnBuscar.addEventListener('click', enseñarDatos);
}
if(btnLimpiar){
  btnLimpiar.addEventListener('click', limpiar);
}
if(btnActualizar){
  btnActualizar.addEventListener('click', actualizar)
}
if(btnTodos){
  btnTodos.addEventListener('click', mostrarProductos);
}
// if(btnBorrar){
//   btnBorrar.addEventListener('click', borrarProducto);
// }

function validar(){
  id = document.getElementById('codigo').value;
  nombreProd = document.getElementById('nombreProducto').value;
  desc = document.getElementById('descripcion').value;
  prec = document.getElementById('precio').value;
  imgNom = document.getElementById('estado').value;
  link = document.getElementById('url').value;
  est = document.getElementById('estado').value;
  
  if(id == 0){
      alert("Por favor, Ingrese un valor válido");
  }
}

btnValidar.addEventListener('click', validar)