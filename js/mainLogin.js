// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-app.js";
import { getDatabase,onValue,ref,set,get,child,update,remove } from "https://www.gstatic.com/firebasejs/9.14.0/firebase-database.js";
import { getAuth, signInWithEmailAndPassword, onAuthStateChanged, signOut} from "https://www.gstatic.com/firebasejs/9.14.0/firebase-auth.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries


// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyB3yHQQnCdhBwKRAV_XaD0WdxiMZf9WU6E",
    authDomain: "recuperacionfinal-46885.firebaseapp.com",
    databaseURL: "https://recuperacionfinal-46885-default-rtdb.firebaseio.com",
    projectId: "recuperacionfinal-46885",
    storageBucket: "recuperacionfinal-46885.appspot.com",
    messagingSenderId: "772661284451",
    appId: "1:772661284451:web:d9b6dbf50f55110789f256"
  };

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();


// Variables generales a usar
var btnIngresar = document.getElementById("boton");
var btnDesconectar = document.getElementById('btnDesconectar');
var btnLimpiar = document.getElementById("btnLimpiarLogin");

var usuario = "";
var contrasena = "";





const auth = getAuth();


function comprobarDatos(){
    usuario = document.getElementById("usuario").value;
    contrasena = document.getElementById("contrasena").value;
}

// Ayuda a comprobar si el usuario ya está conectado
function comprobarLogin(){
    onAuthStateChanged(auth, (user) => {
        if (user) {
          alert("Usuario detectado");
          
        } else {
          
          alert("El usuario no ha iniciado sesión");
          window.location.href="/rExa/html/login.html";
        }
      });
}

if(window.location.href == "https://practica1giancarlo.000webhostapp.com/rExa/html/login.html"){
        window.onload = comprobarLogin();
}

// Comprueba el correo y contraseña
if(btnIngresar){
    btnIngresar.addEventListener('click', (e)=>{
        comprobarDatos();
        signInWithEmailAndPassword(auth, usuario, contrasena)
        .then((userCredential) => {
            const user = userCredential.user;
            alert("Ha iniciado sesión exitosamente")
            window.location.href = "/rExa/html/principal.html";
        })
        .catch((error) => {
            alert("Los datos son incorrectos")
            const errorCode = error.code;
            const errorMessage = error.message;
            limpiar();
        });
    });
}

// IF que desconectará al usuario
if(btnDesconectar){
    btnDesconectar.addEventListener('click', (e)=>{
        signOut(auth).then(() => {
            alert("Desconexión exitosa")
            window.location.href = "/rExa/html/principal.html";
        }).catch((error) => {
            
        })
    })
}




function limpiar(){
    
     document.getElementById("usuario").value = "";
     document.getElementById("contrasena").value= "";
    
}

btnLimpiar.addEventListener('click', limpiar)